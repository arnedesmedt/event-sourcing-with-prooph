<?php

declare(strict_types=1);

namespace App\ES\Infrastructure\Repository;

use App\ES\Model\User\User;
use App\ES\Model\User\UserCollection;
use App\ES\Model\User\ValueObject\UserId;
use Prooph\EventSourcing\Aggregate\AggregateRepository;

final class UserRepository extends AggregateRepository implements UserCollection
{
    public function save(User $user): void
    {
        $this->saveAggregateRoot($user);
    }

    public function get(UserId $userId): ?User
    {
        return $this->getAggregateRoot($userId->toString());
    }
}

<?php

declare(strict_types=1);

namespace App\ES\ProcessManager;

use App\ES\Model\User\Command\SendMailWhenUserRegistered;
use App\ES\Model\User\Event\UserRegistered;
use Prooph\ServiceBus\CommandBus;

final class MailWhenUserRegisteredProcessManager
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(UserRegistered $userRegistered)
    {
        $this->commandBus->dispatch(SendMailWhenUserRegistered::with($userRegistered->email()));
    }
}

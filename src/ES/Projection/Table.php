<?php

declare(strict_types=1);

namespace App\ES\Projection;

final class Table
{
    public const USER = 'read_user';
    public const USER_STAT = 'stat_user';
}

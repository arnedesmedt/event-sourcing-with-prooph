<?php

declare(strict_types=1);

namespace App\ES\Projection\User;

use App\ES\Projection\Table;
use Doctrine\DBAL\Connection;
use Prooph\EventStore\Projection\AbstractReadModel;

final class UserStatReadModel extends AbstractReadModel
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function init(): void
    {
        $tableName = Table::USER_STAT;

        $sql = <<<EOT
CREATE TABLE `$tableName` (
  `total` int(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
EOT;
        $insertSql = sprintf('INSERT INTO `%s` VALUES (0);', $tableName);

        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $statement = $this->connection->prepare($insertSql);
        $statement->execute();
    }

    public function isInitialized(): bool
    {
        $tableName = Table::USER_STAT;

        $sql = "SHOW TABLES LIKE '$tableName';";

        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $result = $statement->fetch();

        return $result !== false;
    }

    public function reset(): void
    {
        $tableName = Table::USER_STAT;

        $sql = "TRUNCATE TABLE $tableName;";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    public function delete(): void
    {
        $tableName = Table::USER_STAT;

        $sql = "DROP TABLE $tableName;";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    public function addUser(): void
    {
        $statement = $this->connection->prepare(sprintf('UPDATE %s SET total = total + 1', Table::USER_STAT));
        $statement->execute();
    }

    public function removeUser(): void
    {
        $statement = $this->connection->prepare(sprintf('UPDATE %s SET total = total - 1', Table::USER_STAT));
        $statement->execute();
    }
}

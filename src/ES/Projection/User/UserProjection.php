<?php

declare(strict_types=1);

namespace App\ES\Projection\User;

use App\ES\Model\User\Event\AccountRemoved;
use App\ES\Model\User\Event\UserNameChanged;
use App\ES\Model\User\Event\UserRegistered;
use Prooph\Bundle\EventStore\Projection\ReadModelProjection;
use Prooph\EventStore\Projection\ReadModelProjector;

final class UserProjection implements ReadModelProjection
{
    public function project(ReadModelProjector $projector): ReadModelProjector
    {
        return $projector->fromStream('event_stream')
            ->when([
                UserRegistered::class => function ($state, UserRegistered $event) {
                    /** @var UserReadModel $readModel */
                    $this
                        ->readModel()
                        ->stack(
                            'insert',
                            [
                                'id' => $event->id()->toString(),
                                'name' => $event->name()->toString(),
                            ]
                        );
                },

                UserNameChanged::class => function ($state, UserNameChanged $event) {
                    $this
                        ->readModel()
                        ->stack(
                            'update',
                            [
                                'name' => $event->name()->toString(),
                            ],
                            [
                                'id' => $event->aggregateId(),
                            ]
                        );
                },

                AccountRemoved::class => function ($state, AccountRemoved $event) {
                    $this
                        ->readModel()
                        ->stack(
                            'deleteModel',
                            [
                                'id' => $event->aggregateId(),
                            ]
                        );
                },
            ]);
    }
}

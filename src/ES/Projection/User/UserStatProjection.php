<?php

declare(strict_types=1);

namespace App\ES\Projection\User;

use App\ES\Model\User\Event\AccountRemoved;
use App\ES\Model\User\Event\UserRegistered;
use Prooph\Bundle\EventStore\Projection\ReadModelProjection;
use Prooph\EventStore\Projection\ReadModelProjector;

final class UserStatProjection implements ReadModelProjection
{
    public function project(ReadModelProjector $projector): ReadModelProjector
    {
        return $projector->fromStream('event_stream')
            ->when([
                UserRegistered::class => function ($state, UserRegistered $event) {
                    /** @var UserStatReadModel $readModel */
                    $this
                        ->readModel()
                        ->stack(
                            'addUser'
                        );
                },

                AccountRemoved::class => function ($state, AccountRemoved $event) {
                    $this
                        ->readModel()
                        ->stack(
                            'removeUser'
                        );
                },
            ]);
    }
}

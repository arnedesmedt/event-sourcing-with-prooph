<?php

declare(strict_types=1);

namespace App\ES\Model\User\ValueObject;

final class UserEmail
{
    /**
     * @var string
     */
    private $email;

    public static function fromString(string $email)
    {
        // eventually check if the email is not empty or...

        return new self($email);
    }

    private function __construct(string $email)
    {
        $this->email = $email;
    }

    public function toString() : string
    {
        return $this->email;
    }
}

<?php

declare(strict_types=1);

namespace App\ES\Model\User\ValueObject;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class UserId
{
    /**
     * @var UuidInterface
     */
    private $uuid;

    public static function fromString(string $uuidString)
    {
        // eventually check if the uuid is valid.

        return new self(Uuid::fromString($uuidString));
    }

    private function __construct(UuidInterface $uuid)
    {
        $this->uuid = $uuid;
    }

    public function toString() : string
    {
        return $this->uuid->toString();
    }
}

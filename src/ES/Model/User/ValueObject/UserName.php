<?php

declare(strict_types=1);

namespace App\ES\Model\User\ValueObject;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

final class UserName
{
    /**
     * @var string
     */
    private $name;

    public static function fromString(string $name)
    {
        // eventually check if the name is not empty or...

        return new self($name);
    }

    private function __construct(string $name)
    {
        $this->name = $name;
    }

    public function toString() : string
    {
        return $this->name;
    }
}

<?php

declare(strict_types=1);

namespace App\ES\Model\User\Handler;

use App\ES\Model\User\Command\RegisterUser;
use App\ES\Model\User\User;
use App\ES\Model\User\UserCollection;

final class RegisterUserHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(UserCollection $userCollection)
    {
        $this->userCollection = $userCollection;
    }

    public function __invoke(RegisterUser $registerUser)
    {
        $user = User::register($registerUser->userId(), $registerUser->userName(), $registerUser->email());

        $this->userCollection->save($user);
    }
}

<?php

declare(strict_types=1);

namespace App\ES\Model\User\Handler;

use App\ES\Model\User\Command\ChangeUserName;
use App\ES\Model\User\User;
use App\ES\Model\User\UserCollection;

final class ChangeUserNameHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(UserCollection $userCollection)
    {
        $this->userCollection = $userCollection;
    }

    public function __invoke(ChangeUserName $changeUserName)
    {
        /** @var User $user */
        $user = $this->userCollection
            ->get($changeUserName->userId());

        var_dump($user->email()->toString());

        $user
            ->changeUserName($changeUserName->userName());

        $this->userCollection->save($user);
    }
}

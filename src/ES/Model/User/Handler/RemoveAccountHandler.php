<?php

declare(strict_types=1);

namespace App\ES\Model\User\Handler;

use App\ES\Model\User\Command\RemoveAccount;
use App\ES\Model\User\User;
use App\ES\Model\User\UserCollection;

final class RemoveAccountHandler
{
    /**
     * @var UserCollection
     */
    private $userCollection;

    public function __construct(UserCollection $userCollection)
    {
        $this->userCollection = $userCollection;
    }

    public function __invoke(RemoveAccount $removeAccount)
    {
        /** @var User $user */
        $user = $this->userCollection
            ->get($removeAccount->userId())
            ->remove();

        $this->userCollection->save($user);
    }
}

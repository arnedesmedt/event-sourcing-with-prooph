<?php

declare(strict_types=1);

namespace App\ES\Model\User\Handler;

use App\ES\Model\User\Command\SendMailWhenUserRegistered;

final class SendMailWhenUserRegisteredHandler
{
    private const MAIL_FILE = __DIR__ . "/../../../../../var/log/mail.log";

    public function __invoke(SendMailWhenUserRegistered $sendMailWhenUserRegistered)
    {
        // Do mailing
        file_put_contents(self::MAIL_FILE, $sendMailWhenUserRegistered->email()->toString(), FILE_APPEND);
    }
}

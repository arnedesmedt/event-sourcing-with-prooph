<?php

declare(strict_types=1);

namespace App\ES\Model\User\Command;

use App\ES\Model\User\ValueObject\UserEmail;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

final class SendMailWhenUserRegistered extends Command implements PayloadConstructable
{
    use PayloadTrait;

    public static function with(UserEmail $email) : self
    {
        return new self([
            'email' => $email->toString(),
        ]);
    }

    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    public function email() : UserEmail
    {
        return UserEmail::fromString($this->payload['email']);
    }
}

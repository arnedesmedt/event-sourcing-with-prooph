<?php

declare(strict_types=1);

namespace App\ES\Model\User\Command;

use App\ES\Model\User\ValueObject\UserId;
use App\ES\Model\User\ValueObject\UserName;
use Assert\Assertion;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

final class ChangeUserName extends Command implements PayloadConstructable
{
    use PayloadTrait;

    protected function setPayload(array $payload) : void
    {
        Assertion::keyExists($payload, 'id');
        Assertion::uuid($payload['id']);

        Assertion::keyExists($payload, 'name');
        Assertion::notEmpty($payload['name']);
        Assertion::string($payload['name']);

        $this->payload = $payload;
    }

    public function userId() : UserId
    {
        return UserId::fromString($this->payload['id']);
    }

    public function userName() : UserName
    {
        return UserName::fromString($this->payload['name']);
    }
}

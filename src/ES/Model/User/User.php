<?php

declare(strict_types=1);

namespace App\ES\Model\User;

use App\ES\Model\User\Event\AccountRemoved;
use App\ES\Model\User\Event\UserNameChanged;
use App\ES\Model\User\Event\UserRegistered;
use App\ES\Model\User\ValueObject\UserEmail;
use App\ES\Model\User\ValueObject\UserId;
use App\ES\Model\User\ValueObject\UserName;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;

final class User extends AggregateRoot
{
    /**
     * @var UserId
     */
    private $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * @var UserEmail
     */
    private $email;

    public static function register(UserId $id, UserName $name, UserEmail $email) : self
    {
        $self = new self();
        $self->recordThat(UserRegistered::withData($id->toString(), $name, $email));

        return $self;
    }

    public function changeUserName(UserName $userName) : self
    {
        $this->recordThat(UserNameChanged::withData($this->aggregateId(), $userName));

        return $this;
    }

    public function remove() : self
    {
        $this->recordThat(AccountRemoved::withData($this->aggregateId()));

        return $this;
    }

    protected function aggregateId(): string
    {
        return $this->id->toString();
    }

    /**
     * Apply given event
     */
    protected function apply(AggregateChanged $event): void
    {
        switch($event->messageName()) {
            case UserRegistered::class:
                $this->whenUserWasRegistered($event);
                break;
            case UserNameChanged::class:
                $this->whenUserNameChanged($event);
                break;
            case AccountRemoved::class:
                $this->whenAccountRemoved();
                break;
        }
    }

    private function whenUserWasRegistered(UserRegistered $event)
    {
        $this->id = $event->id();
        $this->name = $event->name();
        $this->email = $event->email();
    }

    private function whenUserNameChanged(UserNameChanged $event)
    {
        $this->name = $event->name();
    }

    private function whenAccountRemoved()
    {
        // $this->id = null => DON'T DO IT => YOU WILL LOOSE YOUR AGGREGATE ID
        $this->name = null;
        $this->email = null;
    }

    public function id() : UserId
    {
        return $this->id;
    }

    public function name() : ?UserName
    {
        return $this->name;
    }

    public function email() : ?UserEmail
    {
        return $this->email;
    }
}

<?php

declare(strict_types=1);

namespace App\ES\Model\User\Event;

use App\ES\Model\User\ValueObject\UserId;
use App\ES\Model\User\ValueObject\UserName;
use Prooph\EventSourcing\AggregateChanged;

final class AccountRemoved extends AggregateChanged
{
    public static function withData(string $id) : self
    {
        $event = self::occur($id);

        return $event;
    }

    public function id() : UserId
    {
        return UserId::fromString($this->aggregateId());
    }
}

<?php

declare(strict_types=1);

namespace App\ES\Model\User\Event;

use App\ES\Model\User\ValueObject\UserEmail;
use App\ES\Model\User\ValueObject\UserId;
use App\ES\Model\User\ValueObject\UserName;
use Prooph\EventSourcing\AggregateChanged;

final class UserRegistered extends AggregateChanged
{
    public static function withData(string $id, UserName $name, UserEmail $email) : self
    {
        $event = self::occur($id, [
            'name' => $name->toString(),
            'email' => $email->toString(),
        ]);

        return $event;
    }

    public function id() : UserId
    {
        return UserId::fromString($this->aggregateId());
    }

    public function name() : UserName
    {
        return UserName::fromString($this->payload['name']);
    }

    public function email() : UserEmail
    {
        return UserEmail::fromString($this->payload['email']);
    }
}

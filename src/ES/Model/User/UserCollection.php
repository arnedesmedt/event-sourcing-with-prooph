<?php

declare(strict_types=1);

namespace App\ES\Model\User;

use App\ES\Model\User\ValueObject\UserId;

interface UserCollection
{
    public function save(User $user) : void;

    public function get(UserId $userId) : ?User;
}

<?php

declare(strict_types=1);

namespace App\Controller\Api;

use Prooph\Common\Messaging\MessageFactory;
use Prooph\ServiceBus\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class CommandController extends Controller
{
    private const NAME_ATTRIBUTE = 'command_name';

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var MessageFactory
     */
    private $messageFactory;

    public function __construct(CommandBus $commandBus, MessageFactory $messageFactory)
    {
        $this->commandBus = $commandBus;
        $this->messageFactory = $messageFactory;
    }

    public function action(Request $request)
    {
        $command = $this->createCommandFromRequest($request);

        $this->commandBus->dispatch($command);

        return JsonResponse::create(null, Response::HTTP_CREATED);
    }

    private function createCommandFromRequest(Request $request)
    {
        $commandName = $request->attributes->get(self::NAME_ATTRIBUTE);
        $payload = json_decode($request->getContent(), true);

        return $command = $this->messageFactory->createMessageFromArray($commandName, ['payload' => $payload]);
    }
}
